from scipy.stats import uniform, gamma
from matplotlib import pyplot as plt
import numpy as np

a = -1
b = 1
x = np.linspace(a - 1, b + 1, 100)
dist = uniform(loc=a, scale=abs(a - b))
figure, ax = plt.subplots(figsize=(8, 8))
ax.grid(linewidth=0.5)
plt.title('Uniform distribution function')
plt.plot(x, dist.cdf(x), color='r', label=r'F({0}, {1})'.format(a, b))
plt.show()

figure, ax = plt.subplots(figsize=(8, 8))
ax.grid(linewidth=0.5)
plt.title('Uniform density function')
plt.plot(x, dist.pdf(x), color='b', label=r'f({0}, {1})'.format(a, b))
plt.show()

a = 0
b = 10
k = 9
etta = 0.5
x = np.linspace(a - 1, b + 1, 100)
figure, ax = plt.subplots(figsize=(8, 8))
ax.grid(linewidth=0.5)
plt.title('Erlang distribution function')
plt.plot(x, gamma.cdf(x, a=k, scale=etta), color='r', label=r'F({0}, {1})'.format(a, b))
plt.show()

figure, ax = plt.subplots(figsize=(8, 8))
ax.grid(linewidth=0.5)
plt.title('Erlang density function')
plt.plot(x, gamma.pdf(x, a=k, scale=etta), color='b', label=r'f({0}, {1})'.format(a, b))
plt.show()