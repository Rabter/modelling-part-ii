import random


class Generator:
    def __init__(self, min, max):
        self.min = min
        self.max = max

    def generation_time(self):
        return random.randint(self.min, self.max)


class Operator(Generator):
    def __init__(self, min, max):
        super().__init__(min, max)
        self.busy = False


class EventModel:
    def __init__(self, n):
        self.n = n
        self.events = list()
        self._inquiry = Operator(5, 10)
        self.clients = Generator(4, 10)
        self.cheaters = Generator(30, 60)
        self._operators = [Operator(15, 30), Operator(20, 25), Operator(10, 30)]
        self.allotted_time = int(20 / len(self._operators))

    def proceed(self):
        self._add_event([self.clients.generation_time(), "cl"])
        self.processed = 0
        self.inquiry_line = []
        self.operators_line = []
        self.availible_time = 0
        self.total_time = 0
        for operator in self._operators:
            operator.busy = False
        self._inquiry.busy = False
        while self.processed < self.n:
            event = self.events.pop(0)
            if event[1] == "cl":
                self._new_client(event[0])
            elif event[1] == "o":
                self._operator_done(event)
            elif event[1] == "i":
                self._inquery_done(event)
            elif event[1] == "cr":
                self._client_returned(event)
            elif event[1] == "ch":
                self._new_cheater(event)
        return self.total_time

    def _add_event(self, event: list):
        i = 0
        while i < len(self.events) and self.events[i][0] < event[0]:
            i += 1
        self.events.insert(i, event)

    def _new_client(self, time):
        self.inquiry_line.append(time)
        self._inquiry_job(time)
        self._add_event([time + self.clients.generation_time(), "cl"])

    def _new_cheater(self, event):
        event.append(0)  # Did not wait in inquiry
        self.operators_line.append(event)
        self._operator_job(event[0])
        self._add_event([event[0] + self.cheaters.generation_time(), "cr"])

    def _inquiry_job(self, time):
        if not self._inquiry.busy and len(self.inquiry_line) > 0:
            self._inquiry.busy = True
            client_time = self.inquiry_line.pop(0)
            handling_finish_time = time + self._inquiry.generation_time()
            self._add_event([handling_finish_time, "i"])
            while self.availible_time < handling_finish_time:
                self.availible_time += self.allotted_time
            self._add_event([self.availible_time, "cr", handling_finish_time - client_time])

    def _client_returned(self, event):
        # Skipping all cheaters as they go last
        i = len(self.operators_line) - 1
        while i > 0 and self.operators_line[i][1] == "ch":
            i += 1
        self.operators_line.insert(i, event)
        self._operator_job(event[0])

    def _operator_job(self, time):
        if len(self.operators_line) > 0:
            i = 0
            go = True
            while i < len(self._operators) and go:
                if self._operators[i].busy:
                    i += 1
                else:
                    go = False
            if not go:
                self._operators[i].busy = True
                event = self.operators_line.pop(0)
                handling_finish_time = time + self._operators[i].generation_time()
                self.total_time += handling_finish_time - event[0] + event[2]
                self._add_event([handling_finish_time, "o", i])

    def _inquery_done(self, event):
        self._inquiry.busy = False
        self._inquiry_job(event[0])

    def _operator_done(self, event):
        self.processed += 1
        self._operators[event[2]].busy = False
        self._operator_job(event[0])


if __name__ == '__main__':
    n = int(input("Enter clients amount: "))
    model = EventModel(n)
    total_time = model.proceed()
    print("Processed:", n, "\nTotal awaiting time:", total_time, "\nAverage awaiting time:", total_time / n)
