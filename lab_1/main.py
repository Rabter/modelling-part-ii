import random
from scipy.stats import chisquare


maxrand = 1000


class TableRand:
    def __init__(self):
        self.table = []
        numbers = set()
        with open("numbers.txt") as file:
            for line in file:
                numbers.update(set(line.split(" ")[1:-1]))
        numbers.remove("")
        self.table = list(numbers)
        self.index = 0

    def next_rand(self, min, max):
        res = (int(self.table[self.index]) + min) % max
        self.index += 1
        if self.index == len(self.table):
            self.index = 0
        return res


def check_sequence(sequence):
    expected = len(sequence) / maxrand
    quantities = [0] * maxrand
    for num in sequence:
        quantities[num] += 1

    sum = 0
    for i in range(maxrand):
        sum += (quantities[i] - expected) ** 2 / expected
    return sum


if __name__ == "__main__":
    table = TableRand()
    n = int(input("Enter sequences length:\n"))
    chisquare([16, 18, 16, 14, 12, 12])
    rand_seq = []
    table_seq = []
    for i in range(n):
        rand_seq.append(random.randint(0, maxrand - 1))
        table_seq.append(table.next_rand(0, maxrand - 1))

    print("Algorithm criterion: ", check_sequence(rand_seq))
    print("Table criterion: ", check_sequence(table_seq))

    choice = '\0'
    while choice != 'y' and choice != 'n':
        choice = input("Would you like to see the resulting sequences? (y/n):\n")
        if choice != 'y' and choice != 'n':
            print("Input error. ")

    if choice == "y":
        print("Algorithm sequence:\n", rand_seq)
        print("\nTable sequence:\n", table_seq)
