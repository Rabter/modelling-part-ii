from builtins import int

import numpy

def SLAE_coefitients(intensity_matrix, n):
    res = numpy.zeros((n, n))

    for state in range(n - 1):
        for col in range(n):
            res[state, state] -= intensity_matrix[state][col]
        for row in range(n):
            res[state, row] += intensity_matrix[row][state]

    for state in range(n):
        res[n - 1, state] = 1

    return res


n = int(input("Enter states amount: "))

print("Enter intensity matrix")
intensity = [0] * n
for i in range(n):
    intensity[i] = list(map(float, input().split(' ')))

print("Matrix:")
for row in intensity:
    print(row)

SLAE_right = [0] * n
SLAE_right[-1] = 1

probabilities = numpy.linalg.solve(SLAE_coefitients(intensity, n), SLAE_right)

print("Probabilities:")
print(probabilities)
