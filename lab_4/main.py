from numpy.random import gamma
import random


class Generator:
    def __init__(self, a:float, b:float):
        self.a = a
        self.b = b

    def generation_time(self):
        return self.a + (self.b - self.a) * random.random()


class Processor:
    def __init__(self, k, theta):
        self.k = k
        self.theta = theta

    def generation_time(self):
        return gamma(self.k, self.theta)


class EventModel:
    def __init__(self, generator=Generator(1, 10), processor=Processor(9, 0.5), total_apps=0, repeat=0):
        self.generator = generator
        self.processor = processor
        self.total_apps = total_apps
        self.repeat = repeat

    def proceed(self):
        processed = 0
        self.queue = maxlen = 0
        self.events = [[self.generator.generation_time(), 'g']]
        self.free = True
        while processed < self.total_apps:
            event = self.events.pop(0)
            if event[1] == 'g':
                maxlen = self._generate(event[0], maxlen)
            elif event[1] == 'p':
                processed += 1
                if random.randint(1, 100) <= self.repeat:
                    self.queue += 1
                self._process(event[0])

        return maxlen

    def _add_event(self, event: list):
        i = 0
        while i < len(self.events) and self.events[i][0] < event[0]:
            i += 1
        self.events.insert(i, event)

    def _generate(self, time, maxlen):
        self.queue += 1
        if self.queue > maxlen:
            maxlen = self.queue
        self._add_event([time + self.generator.generation_time(), 'g'])
        if self.free:
            self._process(time)
        return maxlen

    def _process(self, time):
        if self.queue > 0:
            self.queue -= 1
            self._add_event([time + self.processor.generation_time(), 'p'])
            self.free = False
        else:
            self.free = True


class StepByStepModel:
    def __init__(self, step, generator=Generator(1, 10), processor=Processor(9, 0.5), total_apps=0, repeat=0):
        self.step = step
        self.generator = generator
        self.processor = processor
        self.total_apps = total_apps
        self.repeat = repeat

    def proceed(self):
        t_curr = self.step
        processed = 0
        t_gen = self.generator.generation_time()
        queue = maxlen = 0
        t_gen_prev = t_proc = 0
        self.free = True
        while processed < self.total_apps:
            if t_curr > t_gen:
                queue += 1
                if queue > maxlen:
                    maxlen = queue
                t_gen_prev = t_gen
                t_gen += self.generator.generation_time()

            if t_curr > t_proc:
                if queue > 0:
                    was_free = self.free
                    if self.free:
                        self.free = False
                    else:
                        processed += 1
                        if random.randint(1, 100) <= self.repeat:
                            queue += 1
                    queue -= 1
                    if was_free:
                        t_proc = t_gen_prev + self.processor.generation_time()
                    else:
                        t_proc += self.processor.generation_time()
                else:
                    self.free = True
            t_curr += self.step
        return maxlen


if __name__ == "__main__":
    a = float(input("Enter lowest generation time: "))
    b = float(input("Enter highest generation time: "))

    gen = Generator(a, b)

    k = float(input("Enter processor time's shape (k): "))
    theta = float(input("Enter processor time's size (theta): "))
    proc = Processor(k, theta)

    total_apps = int(input("Enter total apps amount: "))

    repeat = int(input("Enter a chance of application reenering (%): "))

    if input("Input 0 to test step-by-step or non-zero to test by events: ") == '0':
        step = float(input("Enter step: "))
        model = StepByStepModel(step, gen, proc, total_apps, repeat)
    else:
        model = EventModel(gen, proc, total_apps, repeat)

    print("Maximal queue size:", model.proceed())
